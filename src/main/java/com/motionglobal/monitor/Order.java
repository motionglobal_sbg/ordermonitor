package com.motionglobal.monitor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Order implements Serializable {

	private static final long serialVersionUID = 1L;
	private String code;
	private Date createTime;
	private Date updateTime;
	private int paymentStatus = -1;
	private int orderStatus = -1;
	private int salesOrderStatus = -1;
	private int paymentStatus2 = -1;
	private int orderStatus2 = -1;
	private int salesOrderStatus2 = -1;
	private String paymentGateway;
	private String transactionId;
	private String sourceDatabase;
	private Boolean synced = false;

	public void setSynced(Boolean value) {
		synced = value;
	}

	public Boolean getSynced() {
		return synced;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String value) {
		code = value;
	}
	
	public String getSourceDatabase() {
		return sourceDatabase;
	}
	
	public void setSourceDatabase(String value) {
		sourceDatabase = value;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	
	public void setCreateTime(Date value) {
		createTime = value;
	}
	
	public Date getUpdateTime() {
		return updateTime;
	}
	
	public void setUpdateTime(Date value) {
		updateTime = value;
	}
	
	public int getPaymentStatus() {
		return paymentStatus;
	}
	
	public void setPaymentStatus(int value) {
		paymentStatus = value;
	}
	
	public int getOrderStatus() {
		return orderStatus;
	}
	
	public void setOrderStatus(int value) {
		orderStatus = value;
	}
	
	public int getSalesOrderStatus() {
		return salesOrderStatus;
	}
	
	public void setSalesOrderStatus(int value) {
		salesOrderStatus = value;
	}
	
	public int getPaymentStatus2() {
		return paymentStatus2;
	}
	
	public void setPaymentStatus2(int value) {
		paymentStatus2 = value;
	}
	
	public int getOrderStatus2() {
		return orderStatus2;
	}
	
	public void setOrderStatus2(int value) {
		orderStatus2 = value;
	}
	
	public int getSalesOrderStatus2() {
		return salesOrderStatus2;
	}
	
	public void setSalesOrderStatus2(int value) {
		salesOrderStatus2 = value;
	}
	
	public String getPaymentGateway() {
		return paymentGateway;
	}
	
	public void setPaymentGateway(String value) {
		paymentGateway = value;
	}
	
	public String getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(String value) {
		transactionId = value;
	}
	
	public static boolean Contains(List<Order> orders, Order order) {
		for (Order o : orders) {
			if (o.getCode().equals(order.getCode())) {
				o.setPaymentStatus2(order.getPaymentStatus());
				o.setOrderStatus2(order.getOrderStatus());
				o.setSalesOrderStatus2(order.getSalesOrderStatus());
				order.setPaymentStatus2(o.getPaymentStatus());
				order.setOrderStatus2(o.getOrderStatus());
				order.setSalesOrderStatus2(o.getSalesOrderStatus());
				return true;
			}
		}
		
		return false;
	}
}