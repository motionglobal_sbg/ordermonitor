package com.motionglobal.monitor;

import java.text.SimpleDateFormat;
import java.util.Date;

class Configuration {

    static boolean production = false;
    static long ignoreHours = 2;
    static long checkingDays = 5;

    static String emailTo = production ? "grace.xu@motionglobal.com" : "wayne.chen@motionglobal.com";
    static String emailCc = production ? "wayne.chen@motionglobal.com" : "";
    static String emailBcc = "";
    static String emailFrom = "itom@motionglobal.com";
    static String emailFromName = "OM Notification";
    static String emailSubject = "Missing Payment Orders Found - Total: (__count__)";
    static String emailStyleTableHeader = "border-collapse: collapse;";
    static String emailStyleTableCell = "border: 1px solid #ccc; padding: 2px;";

    static String urlPattern = "jdbc:mysql://__server__:3306/sbg2012";
    static String user = "dev_select_0607";
    static String password = "xmBHPiTZw7oPByCEyc5T@dev";
    static String exportFile = "output/%s_%s.csv";
    static String pullOrderApi = "http://om.motionglobal.com/java-api/order-track";
    static String sendGridApiKey = "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";

    static SimpleDateFormat fullDatetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    static SimpleDateFormat shortDate = new SimpleDateFormat("yyyy-MM-dd");

    static String sqlGetOrdersByTime = "select order_code, " +
            "create_time, update_time, " +
            "payment_gateway, transaction_id, " +
            "payment_status, order_status, sales_order_status " +
            "from sbg_order " +
            "where create_time >= '__start_datetime__' " +
            "and create_time <= '__end_datetime__' " +
            "and (payment_status = 1 or __all_payment_status__) " +
            "limit 50000";

    static String sqlGetOrdersById = "select order_code, " +
            "create_time, update_time, " +
            "payment_gateway, transaction_id, " +
            "payment_status, order_status, sales_order_status " +
            "from sbg_order " +
            "where order_code in (__missing_orders__) " +
            "limit 50000";

    // HK internal IP: 192.168.100.139 | HK external IP: 180.150.153.139
    static String[][] sbgServers = new String[][]{
            {"AU_03", "119.9.18.96"},
            // {"US_06", "72.32.82.92"},
            {"EU_09", "94.236.17.220"},
            {"EU_10", "94.236.17.218"},
            {"HK_02", production ? "192.168.100.139" : "180.150.153.139"},
            {"CN_11", "54.223.249.166"}
    };

    // HK internal IP: 192.168.100.71 | OM external IP: 180.150.147.71
    static String[][] omServers = new String[][]{
            {"OM_DB", production ? "192.168.100.71" : "180.150.147.71"}
    };

    static String getNow() {
        return Configuration.fullDatetime.format(new Date());
    }

    static String getFilePath(String fileName) {
        return String.format(exportFile, fileName, shortDate.format(new Date()).replace("-", "_"));
    }

    /*
    static void load() throws FileNotFoundException {

        InputStream input = null;

        try {
            // load a properties file
            input = new FileInputStream(System.getProperty("user.dir") + "/src/main/resources/config.properties");
            Properties prop = new Properties();
            prop.load(input);

            // get the property value and print it out
            production = Boolean.parseBoolean(prop.getProperty("Settings.App.Debug"));
            ignoreHours = Long.parseLong(prop.getProperty("Settings.Scope.IgnoreHours"));
            checkingDays = Long.parseLong(prop.getProperty("Settings.Scope.CheckingDays"));

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    */
}
