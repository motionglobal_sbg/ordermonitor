package com.motionglobal.monitor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.List;

public class run {
    public static void main(String[] args) throws SQLException, IOException {

        PrintStream console = System.out;
        String lastOutputLine;

        /*
        try {
            Configuration.load();
        } catch (FileNotFoundException e) {
            lastOutputLine = "Failed to load configuration file.";
            console.println(lastOutputLine);
            return;
        }
        */

        List<Order> outOfSyncedOrders;
        List<Order> latestStatusOrders;
        List<Order> recoverFailedOrders;

        // step 1: for SBG, only get paid orders
        lastOutputLine = Configuration.getNow() + " : Start to get SBG paid orders...";
        console.println(lastOutputLine);
        List<Order> sbgOrders = Processor.getOrders(Configuration.sbgServers, true);

        // step 2: for OM, get both all orders even it is unpaid or cancelled
        lastOutputLine = Configuration.getNow() + " : Start to get OM all orders...";
        console.println(lastOutputLine);
        List<Order> omOrders = Processor.getOrders(Configuration.omServers, true);

        // step 3: get status out-of-synced order list
        lastOutputLine = Configuration.getNow() + " : Start to check missing orders...";
        console.println(lastOutputLine);
        outOfSyncedOrders = Processor.compare(sbgOrders, omOrders);

        console.println();
        lastOutputLine = String.format("%s%s %s %s", Configuration.getNow(), ":", outOfSyncedOrders.size(), "out-of-synced order(s) found!");
        console.println(lastOutputLine);

        if (outOfSyncedOrders.size() == 0)
            return;

        lastOutputLine = Processor.formatOutputText(outOfSyncedOrders);

        console.println();
        console.println(lastOutputLine);

        // step 4: check how many orders are totally missing in OM (not just status out-of-synced)
        System.out.println("Do you want to pull orders from SBG again?");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input = br.readLine();

        if (Configuration.production || (input.length() > 0 && input.substring(0, 1).toUpperCase().equals("Y"))) {

            System.out.println("Start to pull orders from SBG ...");
            List<Order> ordersMissingInOm = Processor.countMissingOrder(outOfSyncedOrders);

            if (ordersMissingInOm.size() > 0) {

                console.println();
                console.println(String.format("%s%s %s", Configuration.getNow(), ":", "Start pulling order from SBG..."));

                String processResult;

                // Step 4.1: try to re-pull the order from SBG
                for (Order o : ordersMissingInOm) {
                    try {
                        console.print(String.format("%s%s %s", Configuration.getNow(), ":", "Pulling order[" + o.getCode() + "]..."));
                        processResult = Processor.pullOrderFromSbg(o.getCode());

                        console.println(processResult);
                    } catch (Exception e) {
                        console.println("Exception occurs when pulling order[" + o.getCode() + "] info from SBG...");
                        console.println(e.getMessage());
                        console.println();
                    }
                }
            }
        }

        // step 5: reload out-of-synced orders from OM, some of them might be fixed in previous step 4.1
        latestStatusOrders = Processor.getOrdersById(Configuration.omServers, outOfSyncedOrders);
        recoverFailedOrders = Processor.getRecoverFailedOrders(outOfSyncedOrders, latestStatusOrders);

        console.println();

        // publish the info on console
        lastOutputLine = String.format("%s%s %s %s", Configuration.getNow(), ":", recoverFailedOrders.size(), "missing payment status order(s) found!");
        console.println(lastOutputLine);

        lastOutputLine = Processor.formatOutputText(recoverFailedOrders);

        // publish the info on console
        console.println();
        console.println(lastOutputLine);

        // publish the info to file
        // console.println(Processor.logResults(lastOutputLine, "recover_failed_order"));

        // publish the info to email
        if (Configuration.production) {
            console.println();
            console.println("Sending notification email... ");
            lastOutputLine = Processor.sendNotification(lastOutputLine, recoverFailedOrders.size());
        }

        console.println(lastOutputLine);
    }
}
