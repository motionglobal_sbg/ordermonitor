package com.motionglobal.monitor;

import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class Processor {

    static List<Order> getOrders(String[][] servers, boolean paidOnly) throws SQLException {

        List<Order> results = new ArrayList<>();

        long checkingDays = Configuration.checkingDays;
        long ignoreHours = Configuration.ignoreHours;

        // start to load existing orders from each SBG database
        for (String[] server : servers) {
            List<Order> ordersFound = DataAccess.getOrders(server[0], server[1], checkingDays, ignoreHours, paidOnly);

            // put the result together
            results.addAll(ordersFound);

            // print out the result summary
            System.out.println(Configuration.fullDatetime.format(new Date()) +
                    " : " + ordersFound.size() + " order(s) found. Total: " +
                    results.size() + " order(s).");
        }

        return results;
    }

    static List<Order> getOrdersById(String[][] servers, List<Order> orders) throws SQLException {

        List<Order> results = new ArrayList<>();

        String orderList = getOrderQuery(orders);

        // start to load existing orders from each SBG database
        for (String[] server : servers) {
            List<Order> ordersFound = DataAccess.getOrdersById(server[0], server[1], orderList);

            if (ordersFound != null && ordersFound.size() > 0) {
                // put the result together
                results.addAll(ordersFound);

                // print out the result summary
                System.out.println(Configuration.fullDatetime.format(new Date()) +
                        " : " + orders.size() + " order(s) found. Total: " +
                        results.size() + " order(s).");
            }
        }

        return results;
    }

    static List<Order> getRecoverFailedOrders(List<Order> outOfSyncedOrders, List<Order> missingPaymentOrders) {
        List<Order> result = new ArrayList<>();

        for (Order o : outOfSyncedOrders) {
            if (Order.Contains(missingPaymentOrders, o)) {
                if (o.getPaymentStatus2() < 2)
                    result.add(o);
            }
        }

        return result;
    }

    static List<Order> compare(List<Order> sbgOrders, List<Order> omOrders) throws SQLException {
        // initial a list to store the differences
        List<Order> results = new ArrayList<>();

        if (sbgOrders != null && omOrders != null) {

            // if found the difference, put it in the result list
            for (Order sbgOrder : sbgOrders) {
                if (!Order.Contains(omOrders, sbgOrder)) {
                    results.add(sbgOrder);
                }
            }
        }

        String missingOrderList = getOrderQuery(results);
        String omServer = Configuration.omServers[0][0];
        String omIP = Configuration.omServers[0][1];
        List<Order> missingOrders = DataAccess.getOrdersById(omServer, omIP, missingOrderList);
        fillOmOrderDetails(results, missingOrders);

        return results;
    }

    private static void fillOmOrderDetails(List<Order> sbgOrders, List<Order> omOrders) {

        if (sbgOrders != null && omOrders != null) {

            // if found the difference, put it in the result list
            for (Order sbgOrder : sbgOrders) {
                for (Order omOrder : omOrders) {
                    if (sbgOrder.getCode().equals(omOrder.getCode())) {
                        sbgOrder.setPaymentStatus2(omOrder.getPaymentStatus());
                        sbgOrder.setOrderStatus2(omOrder.getOrderStatus());
                        sbgOrder.setSalesOrderStatus2(omOrder.getSalesOrderStatus());
                    }
                }
            }
        }

    }

    private static String getOrderQuery(List<Order> orders) {

        if (orders == null)
            return null;

        StringBuilder orderNumberList = new StringBuilder();

        for (Order order : orders) {
            orderNumberList.append(String.format("%s%s%s", "\"", order.getCode(), "\","));
        }

        String orderListQuery = orderNumberList.toString();
        int stringLength = orderListQuery.length();

        if (stringLength > 1) {
            orderListQuery = orderListQuery.substring(0, stringLength - 1);
        }

        return orderListQuery;
    }

    static String formatOutputText(List<Order> orders) {

        StringBuilder result = new StringBuilder();
        String orderLine;
        int counter = 0;

        for (Order o : orders) {

            if (result.length() == 0) {
                String header = String.format(
                        "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s%s",
                        "server_name",
                        "order_code",
                        "create_time",
                        "update_time",
                        "payment_gateway",
                        "transaction_id",
                        "payment_status",
                        "order_status",
                        "sales_order_status",
                        "payment_status2",
                        "order_status2",
                        "sales_order_status2",
                        "\r\n"
                );

                result.append(header);
            }

            orderLine = String.format(
                    "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s%s",
                    o.getSourceDatabase(),
                    o.getCode(),
                    o.getCreateTime() == null ? "" : Configuration.fullDatetime.format(o.getCreateTime()),
                    o.getUpdateTime() == null ? "" : Configuration.fullDatetime.format(o.getUpdateTime()),
                    o.getPaymentGateway(),
                    o.getTransactionId(),
                    o.getPaymentStatus(),
                    o.getOrderStatus(),
                    o.getSalesOrderStatus(),
                    o.getPaymentStatus2(),
                    o.getOrderStatus2(),
                    o.getSalesOrderStatus2(),
                    "\r\n"
            );

            counter++;

            result.append(orderLine);

            if (counter >= 100)
                break;
        }

        return result.toString();
    }

    static String logResults(String result, String fileName) {

        if (result == null || result.length() == 0) {
            return "No file is exported.";
        }

        String output = DataAccess.WriteFile(result, fileName);
        return "File is successful exported to " + output;
    }

    static String pullOrderFromSbg(String orderCode) throws IOException {
        if (orderCode == null || orderCode.length() == 0)
            throw new IllegalArgumentException("Illegal orderCode!");

        String result = null;
        InputStream instream = null;

        try {

            HttpClient httpclient = HttpClientBuilder.create().build();
            HttpPost httppost = new HttpPost(Configuration.pullOrderApi);

            // Request parameters and other properties.
            List<NameValuePair> params = new ArrayList<>(1);
            params.add(new BasicNameValuePair("orderCode", orderCode));
            UrlEncodedFormEntity url = new UrlEncodedFormEntity(params, "UTF-8");
            httppost.setEntity(url);

            //Execute and get the response.
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();

            instream = entity.getContent();
            result = IOUtils.toString(instream, "UTF-8");
        } catch (Exception e) {
            result = e.getMessage();
        } finally {
            if (instream != null)
                try {
                    instream.close();
                } catch (Exception ex) {
                    // do nothing except print exception message
                    System.out.println(ex.getMessage());
                }
        }

        return result;
    }

    static List<Order> countMissingOrder(List<Order> orders) {

        List<Order> result = new ArrayList<>();

        for (Order o : orders) {
            if (o.getPaymentStatus2() == -1)
                result.add(o);
        }

        return result;
    }

    static String sendNotification(String mailContent, int count) {
        SendGrid sendgrid = new SendGrid(Configuration.sendGridApiKey);

        SendGrid.Email email = new SendGrid.Email();
        email.addTo(Configuration.emailTo.split(";"));
        email.addCc(Configuration.emailCc.split(";"));
        email.addBcc(Configuration.emailBcc.split(";"));
        email.setFrom(Configuration.emailFrom);
        email.setFromName(Configuration.emailFromName);
        email.setSubject(Configuration.emailSubject.replaceAll("__count__", Integer.toString(count)));
        email.setHtml(getHtmlTable(mailContent));

        try {
            SendGrid.Response response = sendgrid.send(email);
            return response.getMessage();
        }
        catch (SendGridException e) {
            return e.getMessage();
        }
    }

    private static String getHtmlTable(String text) {

        if (text != null && text.length() > 0) {

            int rowIndex = 0;
            int lineIndex = 0;
            String[] lines = text.split("\\r\\n");
            StringBuilder htmlTable = new StringBuilder();

            htmlTable.append(String.format("<table style='%1s'>", Configuration.emailStyleTableHeader));

            for (String line : lines) {
                lineIndex++;
                if (!line.equals("")) {

                    String[] fields = line.split("\\t");
                    htmlTable.append("<tr>");
                    rowIndex = 0;

                    for (String field : fields) {
                        rowIndex++;
                        htmlTable.append(String.format("%s%s%s", "<td style='" + Configuration.emailStyleTableCell + "'>", field, "</td>"));

                        if (rowIndex >= 9)
                            break;
                    }

                    htmlTable.append("</tr>");
                }
            }

            htmlTable.append("</table>");

            return htmlTable.toString();
        } else {
            return "";
        }
    }
}
