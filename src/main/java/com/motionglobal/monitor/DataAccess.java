package com.motionglobal.monitor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mysql.jdbc.Connection;

class DataAccess {

    static List<Order> getOrders(String serverName, String serverIP, long checkingDays, long ignoreHours, boolean paidOnly) throws SQLException {

        System.out.println(Configuration.fullDatetime.format(new Date()) + " : Start reading " + serverName + " ...");

        long longToday = new Date().getTime();
        long longStartDate = longToday - (checkingDays * 24 * 60 * 60 * 1000);
        long longEndDate = longToday - (ignoreHours * 60 * 60 * 1000);
        Date startDate = new Date();
        Date endDate = new Date();
        startDate.setTime(longStartDate);
        endDate.setTime(longEndDate);

        String url = Configuration.urlPattern.replaceAll("__server__", serverIP);
        String sql = Configuration.sqlGetOrdersByTime;
        sql = sql.replaceAll("__start_datetime__", Configuration.shortDate.format(startDate));
        sql = sql.replaceAll("__end_datetime__", Configuration.fullDatetime.format(endDate));
        sql = sql.replaceAll("__all_payment_status__", Boolean.toString(!paidOnly));

        return loadOrders(serverName, url, sql);
    }

    static List<Order> getOrdersById(String serverName, String serverIP, String missingOrderList) throws SQLException {

        String url = Configuration.urlPattern.replaceAll("__server__", serverIP);
        String sql = Configuration.sqlGetOrdersById;

        if (missingOrderList != null && missingOrderList.length() > 0) {
            sql = sql.replaceAll("__missing_orders__", missingOrderList);
            return loadOrders(serverName, url, sql);
        } else {
            return null;
        }
    }

    static List<Order> loadOrders(String serverName, String url, String sql) throws SQLException {

        List<Order> orders = new ArrayList<Order>();

        Connection conn = null;
        ResultSet rs = null;

        try {
            conn = (Connection) DriverManager.getConnection(url, Configuration.user, Configuration.password);
            rs = conn.createStatement().executeQuery(sql);

            while (rs.next()) {
                Order o = new Order();
                populateOrder(rs, o);
                o.setSourceDatabase(serverName);
                orders.add(o);
            }
        } catch (SQLException e) {
            System.out.println("Retrieve data from database failed!");
            System.out.println("SQL Query: " + sql);
            System.out.println("Error Code: " + e.getErrorCode());
            System.out.println("SQL State: " + e.getSQLState());
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Retrieve data from database failed!");
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (rs != null && !rs.isClosed())
                rs.close();

            if (conn != null && !conn.isClosed())
                conn.close();
        }

        return orders;
    }

    private static void populateOrder(ResultSet rs, Order o) throws SQLException {
        o.setCode(rs.getString("order_code"));
        o.setCreateTime(rs.getTimestamp("create_time"));
        o.setUpdateTime(rs.getTimestamp("update_time"));
        o.setPaymentGateway(rs.getString("payment_gateway"));
        o.setTransactionId(rs.getString("transaction_id"));
        o.setPaymentStatus(rs.getInt("payment_status"));
        o.setOrderStatus(rs.getInt("order_status"));
        o.setSalesOrderStatus(rs.getInt("sales_order_status"));
    }

    static String WriteFile(String text, String fileName) {
        String output = System.getProperty("user.dir") + "\\" + Configuration.getFilePath(fileName);
        Path file = Paths.get(output);

        try {
            Files.write(file, text.replace("\t", ",").getBytes(), StandardOpenOption.CREATE);
        } catch (IOException e) {
            System.out.println("Write the missing orders to file failed!");
            e.printStackTrace();
        }

        return output;
    }
}
